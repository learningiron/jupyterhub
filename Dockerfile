FROM jupyter/scipy-notebook

USER root

RUN conda install --quiet --yes \
    'pandas' 'nltk' \
    'tensorflow' 'keras' 'theano'

COPY misc/requirements.pip /tmp/

RUN pip install --upgrade pip \
    && pip install https://github.com/Codealist/CharSplit/archive/v1.3.5.1.tar.gz \
    && pip install -r /tmp/requirements.pip

USER $NB_USER
